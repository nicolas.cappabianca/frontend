The Knuth engineering team will begin implementing [Double-spend proofs](https://gitlab.com/-/snippets/1883331) on the node's code.

The motivation to implement this double spend detection mechanism is very well described in the specification:

> The chance of defrauding a merchant by double spending the transaction that is being paid to him is real and the main problem we have today is the fact that without significant infrastructure the merchant won't find out until the block is confirmed.
The low risk of getting caught will make this a problem as Bitcoin Cash becomes more mainstream.
The double-spend-proof is a means with which network participants that have the infrastructure to detect double spends can share that fact so merchants can receive information on their payment app (typically SPV based) in short enough time that the merchant can refuse to provide service or goods to their customer.

In a normal merchant-customer exchange, the customer creates a transaction which he then gives to the merchant. Either directly or through the Bitcoin Cash network. Upon receipt of that transaction, the merchant will be able to recognize and store it, allowing the customer to leave.

This means that the merchant relies on a zero confirmation setup in which the merchant saves a copy of the transaction and will continue to send it to the network until it is confirmed. This works well in the vast majority of cases.

However, the customer may have set up some custom software and immediately after submitting the appropriate transaction to the network, they submit another transaction to a friendly miner. This transaction spends twice the money you initially sent to the merchant.

With Double-spend proofs, any node, and specifically wallet software, can use the information exchanged as a way to alert the network of an attempt to double spend.

While the wallet software is required to implement this protocol as well, the nodes need to implement a series of specific messages and also an API to provide this information to the wallet software.

# Requested Funding

The estimated time for implementing DS-Proofs on Knuth core library is 320 hours.
At a rate of 150usd/hr, the total requested funding for this phase of development is 48 BCH (using BCH price at the time of writing this).

# Knuth's 2020 in numbers

If you want to know what we have been working on for the past year, check out the following article: https://read.cash/@kth/knuths-2020-in-numbers-e900dfc6


# Plan for 2021

- ~~Development of the Knuth Javascript Library~~ (done).
- Implement Double-Spend proofs (this Flipstarter campaign).
- Create our own block explorer on top of the JS library.
- Improve the performance of the node so that it can adapt to the times that come in Bitcoin Cash: 256 MB blocks. For this we have determined that we will need to (1) rewrite the IDB code from scratch and (2) implement a better concurrency scheme.
- Maintain and improve our existing APIs: C++, C, C#, Javascript, ...
- Implement UTXO Commitments.
- Development of the Knuth Rust Library.
- Development of the Knuth Python Library.
- Development of the Knuth Java Library.
- Development of the Knuth Go-lang Library.
- Development of the Knuth Eiffel Library.

The listing above is our entire backlog. Instead of doing a big campaign with all that, we prefer to go on feature-by-feature campaigns basis.
Once we finish the current one, we will move on to the next one. In this way, it allows us to do shorter tasks and since the Bitcoin Cash ecosystem is very dynamic, we can adapt to the demands of the moment.

# Thank you!

Knuth is open source software that needs community support to continue its development. Therefore we would like to thank the generosity of our supporters and that of the entire Bitcoin Cash ecosystem. You are the ones that allow us to move forward with our goal, which is everyone's goal at Bitcoin Cash, which is to be *the Peer-to-Peer Electronic Cash System* for the entire world.

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/Knuth%20Programming%20langs.png" /></p>
