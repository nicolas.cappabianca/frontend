import knuth

def onBlockArrived(errCode, height, incoming, outgoing):
    print(f"{incoming.length} blocks arrived!")

config = knuth.config.settings.getDefault(knuth.config.networkType.mainnet)
node = knuth.node(config)
await node.launch()
print("Knuth node has been launched!")
knuth.chain.subscribeBlockNotifications(onBlockArrived)

height = await node.chain.getLastHeight()

print(f"Current height in local copy: {height.result}")

if node.chain.isStale():
    print("Knuth node is doing IBD...")

await discoverThePowerOfKnuth(node)

