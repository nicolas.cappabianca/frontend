A few weeks ago we launched a Twitter poll to find out our followers' preferences about the next API to be developed by Knuth. These were the results:

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/py-poll-1.jpeg" /></p>

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/py-poll-2.jpeg" /></p>

For this reason the Knuth engineering team will start working on the first version of the Knuth Python library.

Knuth is a Bitcoin Cash full-node written in C++17, but it's not just that.

Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem.

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/py-demo.png" /></p>

So the Python library is about to join our family of libraries: [C++17](https://github.com/k-nuth/node), [C](https://github.com/k-nuth/c-api), [C#](https://github.com/k-nuth/cs-api) and [Javascript and TypeScript](https://github.com/k-nuth/js-api). But this does not end here, our plan is to provide libraries in other popular programming languages: Rust, Java, Go, Eiffel, ...

<!-- <p align="center"><img width="400px" src="https://raw.githubusercontent.com/k-nuth/logos/master/kth-js.png" /></p> -->

<p align="center" width="100%">
    <img width="400px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/kth-py.jpeg" />
</p>

<!-- <img align="center" width="400px" src="https://raw.githubusercontent.com/k-nuth/logos/master/kth-js.png"> -->

We would like you to be able to use the Knuth node as a library in your favorite programming language.

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/Knuth%20Programming%20langs.png" /></p>

# Requested Funding

The estimated time for the development of the Knuth Python library is 240 hours.
At a rate of 0.5 BCH/hr the total requested funding for this phase of development is 120 BCH.


# Our plans, for 2022 and beyond

- Development of the Knuth Python Library (this Flipstarter campaign).
- Maintain and improve our existing APIs: C++, C, C#, Javascript and TypeScript.
- General maintenance of the node software.
- Implement UTXO Commitments.
- Development of the Knuth Rust Library.
- Development of the Knuth Java Library.
- Development of the Knuth Go-lang Library.
- Development of the Knuth Eiffel Library.

The listing above is our entire backlog. Instead of doing a big campaign with all that, we prefer to go on feature-by-feature campaigns basis.
Once we finish the current one, we will move on to the next one. In this way, it allows us to do shorter tasks and since the Bitcoin Cash ecosystem is very dynamic, we can adapt to the demands of the moment.

# Thank you!

Knuth is open source software that needs community support to continue its development. Therefore we would like to thank the generosity of our supporters and that of the entire Bitcoin Cash ecosystem. You are the ones that allow us to move forward with our goal, which is everyone's goal at Bitcoin Cash, which is to be *the Peer-to-Peer Electronic Cash System* for the entire world.
